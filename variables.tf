# @group "AWS"
# @options ["t3.medium", "c5.xlarge"]
variable "instance_type" {
  type = string
  description = "Instance type"
  default = "t3.medium"
}

# @group "AWS"
variable "disk_size" {
  type = number
  description = "Root disk size in GiB"
  default = 50
}

# @group "AWS"
variable "key_name" {
  type = string
  description = "AWS key name"
  default = "xueying"
}

# @group "AWS"
variable "security_group_name" {
  type = string
  description = "Security group Name"
  default = "all-open"
}

variable "private_key" {
  type = string
  description = "SSH private key. Used to connect to the EC2 instance to get the kubeconfig"
  default = ""
  sensitive = true
}

variable "name_prefix" {
  type        = string
  default     = "foo"
}