resource "aws_instance" "k3s" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  subnet_id     = data.aws_subnets.selected.ids.0
  vpc_security_group_ids = [data.aws_security_group.selected.id]
  key_name      = var.key_name
  user_data     = <<-EOF
                  #!/bin/bash
                  set -ex;
                  public_ip=$(curl http://169.254.169.254/latest/meta-data/public-ipv4)

                  function start_k3s() {
                    curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="--tls-san $${public_ip} --node-external-ip $${public_ip}" sh -
                  }

                  function update_kubeconfig() {
                    sed -i "s/127.0.0.1/$${public_ip}/g" /etc/rancher/k3s/k3s.yaml
                  }

                  start_k3s
                  update_kubeconfig
                  EOF
  tags = {
    "Name" = "${var.name_prefix}-k3s"
  }

  root_block_device {
    volume_size = var.disk_size
  }
}

resource "null_resource" "k3s_health_check" {
  depends_on = [
    aws_instance.k3s,
  ]

  triggers = {
    always_run = timestamp()
  }

  provisioner "local-exec" {
    command     = "for i in `seq 1 60`; do curl -k -s $ENDPOINT >/dev/null && exit 0 || true; sleep 5; done; echo TIMEOUT && exit 1"
    interpreter = ["/bin/sh", "-c"]
    environment = {
      ENDPOINT = "https://${aws_instance.k3s.public_ip}:6443"
    }
  }
}

resource "local_sensitive_file" "private_key" {
  content = var.private_key
  filename = "${var.name_prefix}-k3s.pem"
}

data "external" "kubeconfig" {
  depends_on = [null_resource.k3s_health_check]
  program = [
    "sh",
    "-c",
    <<-EOT
      set -e
      cat >/dev/null
      echo '{"base64": "'$(
        ssh -o StrictHostKeyChecking=no -i ${local_sensitive_file.private_key.filename} \
              ubuntu@${aws_instance.k3s.public_ip} \
              'sudo cat /etc/rancher/k3s/k3s.yaml | base64 -w0'
            )'"}'
    EOT
  ]
}
