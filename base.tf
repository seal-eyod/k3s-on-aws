data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}


data "aws_security_group" "selected" {
  name = var.security_group_name
}

data "aws_subnets" "selected" {
  filter {
    name   = "vpc-id"
    values = [data.aws_security_group.selected.vpc_id]
  }
}