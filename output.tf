output "k3s_ip" {
  description = "K3S public IP"
  value = "https://${aws_instance.k3s.public_ip}"
}

output "kubeconfig" {
  value = base64decode(data.external.kubeconfig.result.base64)
}