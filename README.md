# K3S on AWS

This module will deploy a K3S cluster on AWS using Terraform.

## Providers

| Name | Version |
|-----|---------|
| aws | n/a |

## Inputs

| Name                  | Description |   Type   |    Default    | Required |
|-----------------------|-------------|:--------:|:-------------:|:--------:|
| instance\_type        | n/a         | `string` | `"t3.medium"` |    no    |
| disk\_size            | n/a         | `number` |     `50`      |    no    |
| security\_group\_name | n/a         | `string` | `"all-open"`  |    no    |
| name\_prefix          | n/a         | `string` |    `"foo"`    |    no    |

## Outputs

| Name | Description   |
|------|---------------|
| k3s_ip | K3S public IP |